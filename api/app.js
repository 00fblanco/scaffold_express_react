const express = require('express')
const app = express()
const router = express.Router({ strict: true })

app.use(express.static('build'));


router.get('/hi', (req, res) => {
    console.log('->/hi')
    return res.status(200).json({ hi: 'hi'})
});

app.use('/api', router)

app.get('*', (req, res) => {
    console.log('->*')
    return res.sendfile('index.html')
});

app.listen(8080, () => console.log('Startup app in port :8080'));